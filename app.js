var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
const fileUpload = require('express-fileupload');
//Inicializando el objeto express
var app = express();
app.use(bodyParser.json({limit: '50mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false}));
// app.use(bodyParser.urlencoded({extended: false}));
// app.use(bodyParser.json());

var auth = require('./components/auth/routes');
var publications = require('./components/publications/routes');

//CORS ORIGIN MIDELWARE
app.use((req,res,next)=>{
	res.header('Access-Control-Allow-Origin','*');
	res.header('Access-Control-Allow-Headers','X-API-KEY, Origin, X-Requested-Width, Content-Type, Accept,Access-Control-Request-Method,Authorization,territoken');
	res.header('Access-Control-Request-Methods','GET, POST,OPTIONS,PUT,DELETE');
	res.header('Allow','GET, POST,OPTIONS,PUT,DELETE');
	next();
})


app.use(fileUpload());
app.use('/api',auth,publications);
module.exports = app;