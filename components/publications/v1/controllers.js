var ServicesV1 = require('../v1/services');

exports.Publish = function(req,res,next){
	var data = req.body;
	var id = req.user.sub;
   ServicesV1.Publish(id,data)
   .then(function(created){
		res.status(200).send(created);	
	})
	.catch(function(err){
		res.status(500).send(err)
	})
};
exports.settoVisible = function(req,res,next){
	var id = req.params.id;
   ServicesV1.settoVisible(id)
   .then(function(created){
		res.status(200).send(created);	
	})
	.catch(function(err){
		res.status(500).send(err)
	})
};

exports.getPublications = function(req,res,next){
	var id = req.user.sub;
	const skip = req.params.skip ;
	const limit = req.params.limit ;

   ServicesV1.getPublications(id, skip, limit)
   .then(function(list){
		res.status(200).send(list);	
	})
	.catch(function(err){
		res.status(500).send(err)
	})
};
exports.getMyPublications = function(req,res,next){
	var id = req.user.sub;
   ServicesV1.getMyPublications(id)
   .then(function(list){
	   
		res.status(200).send(list);	
	})
	.catch(function(err){
		res.status(500).send(err)
	})
};
exports.getUserPublications= function(req,res,next){
	var id = req.params.id;
   ServicesV1.getMyPublications(id)
   .then(function(list){
		res.status(200).send(list);	
	})
	.catch(function(err){
		res.status(500).send(err)
	})
};
exports.getpublicationdetail = function(req,res,next){
	var id = req.params.id;
	var userid = req.user.sub;
   ServicesV1.getpublicationdetail(id,userid)
   .then(function(list){
		res.status(200).send(list);	
	})
	.catch(function(err){
		res.status(500).send(err)
	})
};
exports.setMedia = function(req,res,next){
	var id = req.params.id;
	var data = req;
   ServicesV1.setMedia(id,data)
   .then(function(created){
		res.status(200).send(created);	
	})
	.catch(function(err){
		res.status(500).send(err)
	})
};
exports.addComment = function(req,res,next){
	var id = req.params.id;
	var userid = req.user.sub;
	var data = req.body;
	var user = req.user;
	console.log("______USER");
	console.log(user);
   ServicesV1.addComment(id,userid,data,user)
   .then(function(created){
		res.status(200).send(created);	
	})
	.catch(function(err){
		res.status(500).send(err)
	})
};
exports.setState = function(req,res,next){
	var id = req.params.id;
	var data = req.body;
   ServicesV1.setState(id,data)
   .then(function(created){
		res.status(200).send(created);	
	})
	.catch(function(err){
		res.status(500).send(err)
	})
};
exports.like = function(req,res,next){
	var id = req.params.id;
	var userid = req.user.sub;
	var like = req.body.like;
	var user = req.user;
   ServicesV1.like(id,userid,like,user)
   .then(function(created){
		res.status(200).send(created);	
	})
	.catch(function(err){
		res.status(500).send(err)
	})
};