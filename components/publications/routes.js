'use strict'
var express = require('express');
var api = express.Router();
var ControllerV1 = require('./v1/controllers');
var userMiddleware = require('../../middlewares/auth');

//Autenticación

api.post('/v1/publications/publish',userMiddleware.isAuth,ControllerV1.Publish);
api.get('/v1/publications/set-to-visible/:id',userMiddleware.isAuth,ControllerV1.settoVisible);
api.get('/v1/publications/list/:skip/:limit',userMiddleware.isAuth,ControllerV1.getPublications);
api.get('/v1/publications/list-my-publications',userMiddleware.isAuth,ControllerV1.getMyPublications);
api.get('/v1/publications/list-user-publications/:id',userMiddleware.isAuth,ControllerV1.getUserPublications);
api.get('/v1/publications/detail/:id',userMiddleware.isAuth,ControllerV1.getpublicationdetail);
api.post('/v1/publications/add-media/:id',userMiddleware.isAuth,ControllerV1.setMedia);
api.post('/v1/publications/add-comment/:id',userMiddleware.isAuth,ControllerV1.addComment);
api.post('/v1/publications/updatepublication/:id',userMiddleware.isAuth,ControllerV1.setState);
api.post('/v1/publications/like-dislike/:id',userMiddleware.isAuth,ControllerV1.like);
// api.post('/v1/publications/remove-dislike/:id',userMiddleware.isAuth,ControllerV1.likedislike);


module.exports = api;
