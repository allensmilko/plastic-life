'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var PublicationSchema =  Schema({
	media:[],
	description:{ type:String, required:false, default: "" },
	register_date:{ type:Date, required:false },
	comments:[{ type:Schema.Types.ObjectId, ref:'Comment'}],
	author:{ type:Schema.Types.ObjectId, ref:'User'},
	likes:[{ type:Schema.Types.ObjectId, ref:'User'}],
	likescount:{type:Number,default:0},
	iliked:{ type:Boolean , default:false },
	status:{type:Number,default:0},
	type:{type:Number}
});
var Publication = mongoose.model('Publication',PublicationSchema);
module.exports.Publication = Publication;

var MediaSchema =  Schema({
	source:{ type:String, required:true },
	preview:{ type:String, required: false },
	type:{ type:String, required:true }
});
var Media = mongoose.model('Media',MediaSchema);
module.exports.Media = Media;

var CommentSchema =  Schema({
	comment:{ type:String, required:true },
	user:{ type:Schema.Types.ObjectId, ref:'User'},
	publication:{ type:Schema.Types.ObjectId, ref:'Publication'}
});
var Comment = mongoose.model('Comment',CommentSchema);
module.exports.Comment = Comment;