/**
 * @apiDefine UserNotFoundError
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "UserNotFound"
 *     }
 */
// Configuration parameter sampleUrl: "http://api.github.com"
 /**
 * @api {get} /api/{{APIVERSION}}/auth/get-access-token @Get Access token
 * @apiVersion 1.0.0
 * @apiName GetAccessToken
 * @apiGroup Auth
 * 
 * @apiSampleRequest /api/v1/auth/get-access-token
 * @apiHeader {String} territoken Token in SHA1.
 * 
 */


 /**
 * @api {get} /api/{{APIVERSION}}/auth/checkemail/:email Check user email
 * @apiVersion 1.0.0
 * @apiName CheckEmail
 * @apiGroup Auth
 * 
 * @apiHeader {String} x_td_auth_token Token authorization.
 * @apiSampleRequest http://34.215.177.171:5000/api/v1/auth/checkemail/:email
 *
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 100,
 *       "is_social": false
 *     }
 */

 /**
 * @api {post} /api/{{APIVERSION}}/auth/resgister Startup user
 * @apiVersion 1.0.0
 * @apiName RegisterUser
 * @apiGroup Auth
 * 
 * @apiSampleRequest http://34.215.177.171:5000/api/v1/auth/register
 *
 * @apiHeader {String} x_td_auth_token Token authorization.
 * @apiParam (Request body) {String} username Full username.
 * @apiParam (Request body) {String} email This email is required for login.
 * @apiParam (Request body) {String} password Password for login.
 * @apiParam (Request body) {String} id_type Type of identity document.
 * @apiParam (Request body) {Number} id_num Number of identification.
 * @apiParam (Request body) {Boolean} is_social Check if is Social login.
 * 
 */

  /**
 * @api {post} /api/{{APIVERSION}}/auth/resgister Startup user
 * @apiVersion 2.0.0
 * @apiName RegisterUser
 * @apiGroup Auth
 * 
 * @apiSampleRequest http://34.215.177.171:5000/api/v2/auth/register
 *
 * @apiHeader {String} x_td_auth_token Token authorization.
 * @apiParam (Request body) {String} username Full username.
 * @apiParam (Request body) {String} email This email is required for login.
 * @apiParam (Request body) {String} password Password for login.
 * @apiParam (Request body) {String} id_type Type of identity document.
 * @apiParam (Request body) {Number} id_num Number of identification.
 * @apiParam (Request body) {Boolean} is_social Check if is Social login.
 * @apiParam (Request body) {String} firetoken for firebase.
 * @apiParam (Request body) {String} type for firebase.
 * 
 */


  /**
 * @api {post} /api/{{APIVERSION}}/auth/login Login user
 * @apiVersion 1.0.0
 * @apiName LoginUser
 * @apiGroup Auth
 * 
 * @apiSampleRequest http://34.215.177.171:5000/api/v1/auth/login
 *
 * @apiHeader {String} x_td_auth_token Token authorization.
 * @apiParam (Request body) {String} email This email is required for login.
 * @apiParam (Request body) {String} password Password for login.
 * 
 */

   /**
 * @api {post} /api/{{APIVERSION}}/auth/login Login user
 * @apiVersion 2.0.0
 * @apiName LoginUser
 * @apiGroup Auth
 * 
 * @apiSampleRequest http://34.215.177.171:5000/api/v2/auth/login
 *
 * @apiHeader {String} x_td_auth_token Token authorization.
 * @apiParam (Request body) {String} email This email is required for login.
 * @apiParam (Request body) {String} password Password for login.
  * @apiParam (Request body) {String} firetoken for firebase.
 * @apiParam (Request body) {String} type for firebase.
 * 
 */

   /**
 * @api {get} /api/{{APIVERSION}}/auth/me Get user data
 * @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup Auth
 * 
 * @apiSampleRequest http://34.215.177.171:5000/api/v1/auth/me
 *
 * @apiHeader {String} x_td_auth_token Token authorization.
 * @apiHeader {String} Authorization User token.
 * 
 */

  /**
 * @api {put} /api/{{APIVERSION}}/auth/update-me Update user info
 * @apiVersion 1.0.0
 * @apiName UpdateUser
 * @apiGroup User
 * 
 * @apiSampleRequest http://34.215.177.171:5000/api/v1/auth/update-me
 *	
 * @apiHeader {String} x_td_auth_token Token authorization.
 * @apiHeader {String} Authorization User token.
 * @apiParam (Request body) {String} username Full username.
 * @apiParam (Request body) {String} email This email is required for login.
 * @apiParam (Request body) {String} password Password for login.
 * @apiParam (Request body) {String} id_type Type of identity document.
 * @apiParam (Request body) {Number} id_num Number of identification.
 * 
 */

 /**
 * @api {put} /api/{{APIVERSION}}/auth/set-pass Update user pass
 * @apiVersion 1.0.0
 * @apiName SetUserPass
 * @apiGroup User
 * 
 * @apiSampleRequest http://34.215.177.171:5000/api/v1/auth/set-pass
 *	
 * @apiHeader {String} x_td_auth_token Token authorization.
 * @apiHeader {String} Authorization User token.
 * @apiParam (Request body) {String} password Password for login.
 * @apiParam (Request body) {String} newpassword Password for login.
 * 
 */

   /**
 * @api {post} /api/{{APIVERSION}}/auth/set-avatar Set avatar
 * @apiVersion 1.0.0
 * @apiName SetAvatar
 * @apiGroup User
 * 
 * @apiSampleRequest http://34.215.177.171:5000/api/v1/auth/set-avatar
 *
 * @apiHeader {String} x_td_auth_token Token authorization.
 * @apiHeader {String} Authorization User token.
 * @apiParam (Request body) {File} avatar User avatar.
 * 
 */

  /**
 * @api {get} /api/{{APIVERSION}}/auth/gen-new-password/:email Generate new user pass
 * @apiVersion 1.0.0
 * @apiName GenerateNewPass
 * @apiGroup Auth
 * 
 * @apiHeader {String} x_td_auth_token Token authorization.
 * @apiSampleRequest http://34.215.177.171:5000/api/v1/auth/gen-new-password/:email
 *
 * 
 */