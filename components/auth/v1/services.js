var User = require('../model').User;
var Country = require('../model').Country;
var City = require('../model').City;
var Clinic = require('../model').Clinic;
var Specialtie = require('../model').Specialties;
var PushNotifications = require('../model').PushNotifications;
var servicev1 = {};
var moment = require('moment');
var bcrypt = require('bcrypt-nodejs');
var service = require('../../../services/auth');
var generator = require('generate-password');
var notifications = require('../../../services/notifications');

var mandrill = require('mandrill-api/mandrill');
const nodemailer = require("nodemailer");



servicev1.startSuperAdmin = function(data){
	return new Promise(function(resolve, reject){
		User.findOne({nombre:data.nombre},function(err,result){
		if(err){
			console.log('Error 1 -------------')
			return reject(err);
		}
		else if (result){
			console.log('Error 2 -------------')
			return resolve(result);
		}
		else{
			console.log('No existe -------------')
			User.create(data,function(err,UserCreado){
				if(err){
					console.log('Error 3 -------------')
					return reject(err);
				}
				else{
					console.log('Error 4 -------------')
					return	resolve(UserCreado);
				}
			})
		}
	})
	})
}
servicev1.checkemail = function(data){
  return new Promise(function(resolve, reject){
User.findOne({email:data.toLowerCase()}, function(err, user) {
        // Comprobar si hay errores
        // Si el User existe o no
        // Y si la contraseña es correcta
        if (err) {
            return reject({
                message: "Ocurrio un error al hacer la peticion",
                error:err
            });
        } else {
           if(user){
            if (!user.isactive) {
              return resolve({code:103,message:"La cuenta no esta activa"});
            }
            else{
              if (user.is_social) {
                return resolve({code:104,is_social:user.is_social,rol:user.rol});
              }
              else{
                return resolve({code:100,is_social:user.is_social,rol:user.rol});
              }
            }
           }
             else{
                    return reject({code:101,message:"El usuaio no existe"});
                }
        }

    });
  })
}
servicev1.register = function (params){
	var currentDate = moment().unix();

   return new Promise(function(resolve, reject){
      var user = new User({
                username : params.username,
                email: params.email,
                password:params.password,
                id_type:params.id_type,
                id_num:params.id_num,
                rol:params.rol,
                register_date:currentDate,
                is_social:params.is_social
  });
      if (!params.avatar) {
        user.avatar="-";
      }
      else{
        user.avatar=params.avatar;
      }
     bcrypt.hash(params.password,null,null,function(err,hash){
        if(err){
            console.log(err)
        }
        else{
            user.password = hash;
        }
    })
        
    User.findOne({email:params.email},(err,userfinded)=>{
        if (userfinded) {
           return resolve({code:404,message:"Este usuario ya existe"});
        }
        else{
            User.create(user,function(err,usercreated) {
          if (err) {
              return reject({code:501,data:{message:"Error al crear el usuario", err: err}});
          }
          else{
            var token = service.createToken(usercreated);
              return resolve({code:100,message:"Usuario creado con exito!",token:token,rol:usercreated.rol});
          }
    })
        }
    })

    
   });
	
}
servicev1.login = function(data){
    console.log("Estan en 1");
	return new Promise(function(resolve, reject){
User.findOne({email:data.email.toLowerCase()}, function(err, user) {
        // Comprobar si hay errores
        // Si el User existe o no
        // Y si la contraseña es correcta
        if (err) {
            return reject({
                message: "Petición denegada, correo o contraseña invalidos"
            });
        } else {
           if(user){
             bcrypt.compare(data.password,user.password,(err,check)=>{
                if(check){
                    var token = new PushNotifications({
                        firetoken:data.firetoken,
                        type:data.type
                    })
                    PushNotifications.create(token,function(err,tokenCreated){
                        if(err){
                            reject(err)
                        }else{
                            User.findByIdAndUpdate(user._id, { $push: { tokens: tokenCreated._id } },function(err,userUpdated) {
                                if(err) {
                                    reject({code:501,data:{message:"Error al crear el usuario", err: err}})
                                }else{
                                    resolve({code:100,token:service.createToken(user),rol:userUpdated.rol,isfirst_time:userUpdated.isfirst_time})
                                }
                            })
                        }
                    })        
                }
                else{
                  console.log(err);
                   return reject({code:401,message:"El usuario no pudo logearse correctamente"});
                }
            })
           }
             else{
                    return reject({code:404,message:"El usuario no existe en nuestra base de datos"});
                }
        }

    });
	})
}
servicev1.generateAccessToken=function(){
  
    return new Promise(function(resolve, reject){
      return resolve  ({token:servicev1.createAccessToken("T3rr1D4t42018Kub04cc3ssT0k3n")});

  })
}
servicev1.generateRandomPass = function(email){
    return new Promise(function(resolve,reject){
        User.findOne({email:email},(err,userfinded)=>{
            if (userfinded) {
                var password = generator.generate({
                    length: 6,
                    numbers: true
                });
                var finalePass={
                    password:""
                };
                bcrypt.hash(password,null,null,function(err,hash){
                    if(err){
                        return resolve({code:500,message:"Error al generar la cntraseña"});
                    }
                    else{
                        finalePass.password = hash;
                        User.findByIdAndUpdate(userfinded._id,finalePass,{new:true}, function(err, list) {
                            if (err) {
                                return reject(err);
                            }
                            else{
                                list.newpass=password;
                                return resolve({list});
                            }
                        });
                    }
                })
                
               
            }
            else{
                return resolve({code:404,message:"Este usuario no existe"});
            }
        })
    })
}
servicev1.setAvatar = function(id,data){
    return new Promise(function(resolve, reject){
        
         if (!data.files){
                return reject({message:"No hay imagenes"});
         }
         else{
            // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
              let avatar = data.files.avatar;
             console.log(id);
              // Use the mv() method to place the file somewhere on your server
              avatar.mv('./uploads/avatars/'+id+'.jpg', function(err,res) {
                    if(err){
                        return reject(err);
                    }
                    else {
                        var localdata = {
                            avatar: '/avatars/'+id+'.jpg'
                        }
                        User.findByIdAndUpdate(id,localdata,{new:true},function(error,result){
                        if(err){
                            return reject(error);
                        }
                        else if (result){
                            return resolve(result);
                        }
                    })
                    }
              });
         }

    })
}

servicev1.search = function(text){
    return new Promise(function(resolve, reject){
        User.find({ $text: { $search: text },rol:1}, function(err, list) {
            if (err) {
                 return reject(err);
            }
            else{
                return resolve(list);
            }
        });
        })
}
servicev1.me = function(id){
  console.log(id);
    return new Promise(function(resolve, reject){
        User.
        findById(id).
        populate('following').    
        exec(function(err, list) {
        if (err) {
             return reject(err);
        }
        else{
          if (list) {
            return resolve(list);
          }
            else{
              return reject({message:"el usuario no existe",code:404});
            }
        }
    });
    })
}

servicev1.getUserNotifications = function(id){
    console.log(id);
      return new Promise(function(resolve, reject){
        User.
        findById(id).
        populate('notifications').
        exec(function (err, list) {
          if (err) {
               return reject(err);
          }
          else{
            if (list) {
              return resolve(list);
            }
              else{
                return reject({message:"el usuario no existe",code:404});
              }
          }
      });
      })
  }

  
servicev1.whomIamfollowing = function(id){
    console.log(id);
      return new Promise(function(resolve, reject){
        User.
        findById(id).
        populate('following').
        exec(function (err, usuario) {
          if (err) {
               return reject(err);
          }
          else{
            if (list) {
              return resolve(list);
            }
              else{
                return reject({message:"el usuario no existe",code:404});
              }
          }
      });
      })
  }
servicev1.updateme =  function(id,data){
    return new Promise(function(resolve, reject){
        
        var user = data;
        user.ultima_modificacion = moment().format();
        User.findByIdAndUpdate(id,user,{new:true}, function(err, list) {
            if (err) {
                return reject(err);
            }
            else{
                return resolve(list);
            }
        });
                
    })
}

servicev1.setPass =  function(id,data){
    return new Promise(function(resolve, reject){
      User.findById(id,function(err,usr){
        bcrypt.compare(data.password,usr.password,(err,check)=>{
                if(check){
                      var pass = data.newpassword;
                      var user = data;
                bcrypt.hash(pass,null,null,function(err,hash){
                    if(err){
                        console.log(err)
                    }
                    else{
                        user.password = hash;
                        console.log(user);
                        User.findByIdAndUpdate(id,user,{new:true}, function(err, list) {
                            if (err) {
                                return reject(err);
                            }
                            else{
                                return resolve(list);
                            }
                        });
                    }
                });
                }
                else{
                  console.log(err);
                   return reject({code:404,message:"Esta no es tu contraseña"});
                }
            })
      })
        
    })
}




servicev1.follow =  function(id,follower){
    return new Promise(function(resolve, reject){
        var notification = {
            title:"Tienes un nuevo seguidor",
            body:"Revisa tu feed de noticias"
        };
        var data = {
            dato:"PRUEBAAAA!"
        };
        var tokens = [];

        User.
        findById(id).
        populate('tokens').
        exec(function (err, usuario) {
          if(err){
              return reject(err);
          }
          else{
              for(var i = 0; i < usuario.tokens.length ; i++){
                  tokens.push(usuario.tokens[i].firetoken);
              }
            
            User.findByIdAndUpdate(id,{ $push: { followers: follower } },{new:true}, function(err, list) {
                if (err) {
                    return reject(err);
                }
                else{
                    notifications.sendPush(notification,data,tokens);
                    User.findByIdAndUpdate(follower,{ $push: { following: id } },{new:true}, function(err, following) {
                        if (err) {
                            return reject(err);
                        }
                        else{
                            return resolve(following);
                        }
                    });    
                }
            });
          }
        });             
    })
}
servicev1.sendPush =  function(id,notify){
    return new Promise(function(resolve, reject){
        var notification = {
            title:notify.title,
            body:notify.message
        };
        var data = {
            dato:"PRUEBAAAA!"
        };
        var tokens = [];

        User.
        findById(id).
        populate('tokens').
        exec(function (err, usuario) {
          if(err){
              return reject(err);
          }
          else{
              for(var i = 0; i < usuario.tokens.length ; i++){
                  tokens.push(usuario.tokens[i].firetoken);
              }
            
              notifications.sendPush(notification,data,tokens)
              .then(function(response){
                  return resolve(response);
              })
              .catch(function(error){
                  return reject(error);
              })
          }
        });             
    })
}
servicev1.unfollow =  function(id,follower){
    return new Promise(function(resolve, reject){
                        User.findByIdAndUpdate(id,{ $pull: { followers: follower } },{new:true}, function(err, list) {
                            if (err) {
                                return reject(err);
                            }
                            else{
                                return resolve(list);
                            }
                        });
    })
}
servicev1.newcountry = function(data){
    return new Promise(function(resolve, reject){
        var country = new Country({
            name:data.name
        });
                        Country.create(country, function(err, list) {
                            if (err) {
                                return reject(err);
                            }
                            else{
                                return resolve(list);
                            }
                        });
    })
}

servicev1.getCountries =  function(){
    return new Promise(function(resolve, reject){
                        Country.find({}, function(err, list) {
                            if (err) {
                                return reject(err);
                            }
                            else{
                                return resolve(list);
                            }
                        });
    })
}
servicev1.newcity = function(data){
    return new Promise(function(resolve, reject){
        var city = new Country({
            name:data.name
        });
                        City.create(city,function(err, list) {
                            if (err) {
                                return reject(err);
                            }
                            else{
                                return resolve(list);
                            }
                        });
    })
}
servicev1.getCities =  function(){
    return new Promise(function(resolve, reject){
                        City.find({},function(err, list) {
                            if (err) {
                                return reject(err);
                            }
                            else{
                                return resolve(list);
                            }
                        });
    })
}
servicev1.newClinic= function(data){
    return new Promise(function(resolve, reject){
        var clinic = new Clinic({
            name:data.name,
            city:data.city
        });
                        Clinic.create(clinic, function(err, list) {
                            if (err) {
                                return reject(err);
                            }
                            else{
                                return resolve(list);
                            }
                        });
    })
}
servicev1.newSpecialite = function(data){
    return new Promise(function(resolve, reject){
        var specialite = new Specialtie({
            name:data.name
        });
                        Specialtie.create(specialite, function(err, list) {
                            if (err) {
                                return reject(err);
                            }
                            else{
                                return resolve(list);
                            }
                        });
    })
}
servicev1.getSpecialites = function(){
    return new Promise(function(resolve, reject){
                        Specialtie.find({}, function(err, list) {
                            if (err) {
                                return reject(err);
                            }
                            else{
                                return resolve(list);
                            }
                        });
    })
}
servicev1.getClinics =  function(id){
    return new Promise(function(resolve, reject){
                        Clinic.find({city:id}, function(err, list) {
                            if (err) {
                                return reject(err);
                            }
                            else{
                                return resolve(list);
                            }
                        });
    })
}
servicev1.filter =  function(data){
    data.rol=1;
    return new Promise(function(resolve, reject){
                        User.find(data,function(err, list) {
                            if (err) {
                                return reject(err);
                            }
                            else{
                                return resolve(list);
                            }
                        });
    })
}


module.exports = servicev1;

