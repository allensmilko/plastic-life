var ServicesV1 = require('../v1/services');
var User = require('../model').User;
var bcrypt = require('bcrypt-nodejs');
const nodemailer = require('nodemailer');
var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');
var CryptoJS = require("crypto-js");

transport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'plasticlifeapp@gmail.com',
            pass: 'plastic2018',
        },
    });
    


exports.register = function(req,res,next){
	var params = req.body;

   ServicesV1.register(params)
   .then(function(user){
	if (user.code==404) {
			res.send(user);
	}
	else{
		res.status(200).send(user);
		  // var resultado = params;
			// var templateDir = path.join(__dirname, '../../../emails', 'register');
			
			// var myTemplate = new EmailTemplate(templateDir);
			// var locals = {
			// 	email:resultado.email,
			// 	name: resultado.username
			//  }
			//  console.log("despues de definir");
			// myTemplate .render(locals , function (err, results) {
			//   // result.html 
			//   // result.text 
			//   console.log("lee el template");
			// 	if (err) {
			// 	   console.error(err);
			// 	}
			// 	// check here what is showing in your result
			// 	transport.sendMail({
			// 		from: 'Plasticlife <milkositas23@gmail.com>',
			// 		to: locals.email,
			// 		subject: 'Bienvenido',
			// 		html: results.html,
			// 		text: results.text
			// 	 })
			// 	 .then(function(response){
			// 		 console.log("Respuesta");
			// 		console.log(response);
			// 		res.status(200).send(user);
			// 	}).catch(function(err){
			// 		console.log(err);
			// 		res.status(500).send(err);
			// 	});
			// 	// res.send(transport);
			// })

	}

		
	})
	.catch(function(err){
		res.status(500).send(err)
	})
};
exports.checkemail = function(req,res,next){
	var email = req.params.email;
	ServicesV1.checkemail(email)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.getCountries = function(req,res,next){
	ServicesV1.getCountries()
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.getCities = function(req,res,next){
	ServicesV1.getCities()
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.newcountry= function(req,res,next){
	var data = req.body;
	ServicesV1.newcountry(data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.newcity = function(req,res,next){
	var data = req.body;
	ServicesV1.newcity(data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.getClinics= function(req,res,next){
	var id = req.params.id;
	ServicesV1.getClinics(id)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.newClinic = function(req,res,next){
	var data = req.body;
	ServicesV1.newClinic(data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.newSpecialite = function(req,res,next){
	var data = req.body;
	ServicesV1.newSpecialite(data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.getSpecialites = function(req,res,next){
	ServicesV1.getSpecialites()
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.filter= function(req,res,next){
	var data = req.body;
	ServicesV1.filter(data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.login = function(req,res,next){
	console.log('controller 1');
   ServicesV1.login(req.body)
	.then(function(result){

		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
};
exports.sendPush = function(req,res,next){
	var id = req.params.id;
	var notify = req.body;
   ServicesV1.sendPush(id,notify)
	.then(function(result){

		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
};
exports.setAvatar = function(req,res,next){
	var id = req.user.sub;
	console.log("------------------------USER");
	console.log(id);
	var data = req;
	ServicesV1.setAvatar(id,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.me = function(req,res,next){
	console.log(req.user);
	var id = req.user.sub;
	ServicesV1.me(id)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.getUserNotifications = function(req,res,next){
	var id = req.user.sub;
	ServicesV1.getUserNotifications(id)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.whomIamfollowing = function(req,res,next){
	console.log(req.user);
	var id = req.user.sub;
	ServicesV1.whomIamfollowing(id)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.getuserById = function(req,res,next){
	console.log(req.user);
	var id = req.params.id;
	ServicesV1.me(id)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
};
exports.follow = function(req,res,next){
	var follower = req.user.sub;
	var id = req.params.id;
	ServicesV1.follow(id,follower)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
};
exports.unfollow = function(req,res,next){
	var follower = req.user.sub;
	var id = req.params.id;
	ServicesV1.unfollow(id,follower)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
};
exports.updateme = function(req,res,next){
	var id = req.user.sub;
	var data = req.body;
	ServicesV1.updateme(id,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.setPass= function(req,res,next){
	var id = req.user.sub;
	var data = req.body;
	ServicesV1.setPass(id,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.search = function(req,res,next){
	var text = req.body.text;
	ServicesV1.search(text)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.generateRandomPass = function(req,res,next){
	var email = req.params.email;
	ServicesV1.generateRandomPass(email)
	.then(function(user){
		console.log(user);
		if(user.code=="404"){
			res.status(200).send(user);
		}
		else{
			var resultado = user.list;
			var templateDir = path.join(__dirname, '../../../emails', 'password');
			
			var myTemplate = new EmailTemplate(templateDir);
			var locals = {
				email:resultado.email,
				name: resultado.username,
				password:resultado.newpass
			 }
			 console.log("despues de definir");
			myTemplate .render(locals , function (err, results) {
			  // result.html 
			  // result.text 
			  console.log("lee el template");
				if (err) {
				   console.error(err);
				}
				// check here what is showing in your result
				transport.sendMail({
					from: 'Plasticlife <milkositas23@gmail.com>',
					to: locals.email,
					subject: 'Hemos cambiado tu contraseña',
					html: results.html,
					text: results.text
				 })
				 .then(function(response){
					 console.log("Respuesta");
					console.log(response);
					res.status(200).send({code:100,message:"Mensaje enviado con exito"});
				}).catch(function(err){
					console.log(err);
					res.status(500).send(err);
				});
				// res.send(transport);
			})
		}
	

})
};



// exports.generateAccessToken = function(req,res,next){
 
//  var SECRET_API = '5f573su1i+=1jt3d';
//                     var type = 'rijndael-128';
//                     var KEY_TOKEN_API = '1e+ertsa97-hsvr2';
//                     var IV = 'r0c8t09cc40s=j9a';
//                     var d = new Date();
//                     var n = d.getTime();
//                     var strKey = SECRET_API + "/" + n;
//                     var key = CryptoJS.enc.Utf8.parse('1e+ertsa97-hsvr2');
//                     var iv = CryptoJS.enc.Utf8.parse('r0c8t09cc40s=j9a');
//                     var encryptedtwo = CryptoJS.AES;
//                     var ciphertext = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(strKey), '1e+ertsa97-hsvr2');
//                     var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(strKey), key, {
//                         keySize: 128,
//                         iv: iv,
//                         padding: CryptoJS.pad.Pkcs7,
//                         mode: CryptoJS.mode.CBC
//                     });
// res.status(200).send(encrypted.toString());

// }