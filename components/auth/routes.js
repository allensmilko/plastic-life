'use strict'
var express = require('express');
var api = express.Router();
var ControllerV1 = require('./v1/controllers');
var userMiddleware = require('../../middlewares/auth');

//Autenticación
api.get('/v1/auth/checkemail/:email',ControllerV1.checkemail);
api.post('/v1/auth/search/',ControllerV1.search);
api.post('/v1/auth/register',ControllerV1.register);
api.post('/v1/auth/login',ControllerV1.login);
api.get('/v1/auth/me',userMiddleware.isAuth,ControllerV1.me);
api.get('/v1/auth/who-i-follow',userMiddleware.isAuth,ControllerV1.whomIamfollowing);
api.post('/v1/auth/update-me',userMiddleware.isAuth,ControllerV1.updateme);
api.post('/v1/auth/set-pass',userMiddleware.isAuth,ControllerV1.setPass);
api.get('/v1/auth/gen-new-password/:email',ControllerV1.generateRandomPass);
api.post('/v1/user/set-avatar', userMiddleware.isAuth,ControllerV1.setAvatar);
api.get('/v1/auth/user/follow/:id',userMiddleware.isAuth,ControllerV1.follow);
api.get('/v1/auth/user/unfollow/:id',userMiddleware.isAuth,ControllerV1.unfollow);
api.get('/v1/auth/user/get-user-by-id/:id',userMiddleware.isAuth,ControllerV1.getuserById);
api.get('/v1/auth/user/get-notifications',userMiddleware.isAuth,ControllerV1.getUserNotifications);
api.post('/v1/auth/user/send-push/:id',userMiddleware.isAuth,ControllerV1.sendPush);
api.post('/v1/geo/new-country',ControllerV1.newcountry);
api.post('/v1/geo/new-city',ControllerV1.newcity);
api.post('/v1/geo/new-clinic',ControllerV1.newClinic);
api.post('/v1/geo/new-specialite',ControllerV1.newSpecialite);
api.get('/v1/geo/countries',ControllerV1.getCountries);
api.get('/v1/geo/cities',ControllerV1.getCities);
api.get('/v1/geo/clinics/:id',ControllerV1.getClinics);
api.get('/v1/geo/specialites',ControllerV1.getSpecialites);
api.post('/v1/geo/filter',ControllerV1.filter);

// api.post('/v1/user/follow', userMiddleware.isAuth,ControllerV1.follow);
// api.get('/v1/user/following', userMiddleware.isAuth,ControllerV1.following);



module.exports = api;