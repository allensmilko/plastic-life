'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var UserSchema =  Schema({
	username:{ type:String, required:true },
	firstname:{ type:String},
	lastname:{ type:String},
	email:{ type:String, required:true },
	password:{ type:String, required:true },
	avatar:{ type:String, required:false },
	isactive:{ type:Boolean, required:false,default:true },
	country:{ type:String, required:false },
	city:{ type:String, required:false },
	phone:{ type:String, required:false },
	age:{ type:String, required:false },
	university:{ type:String, required:false },
	webpage:{ type:String, required:false },
	shortdesc:{ type:String, required:false ,min: 5, max: 160},
	clinic:{type:Schema.Types.ObjectId,ref:'Clinic'},
	register_date:{ type:Date, required:true },
	is_social:{type:Boolean,required:true},
	tokens:[{ type:Schema.Types.ObjectId, ref:'PushNotifications'}],
	followers:[{ type:Schema.Types.ObjectId, ref:'User'}],
	following:[{ type:Schema.Types.ObjectId, ref:'User'}],
	specialties:[{ type:Schema.Types.ObjectId, ref:'Specialties'}],
	specialtie:{ type:Schema.Types.ObjectId, ref:'Specialties'},
	notifications:[{ type:Schema.Types.ObjectId, ref:'Notification'}],
	rol:{type:Number,required:true},
	country:{type:Schema.Types.ObjectId,ref:'Country'},
	city:{type:Schema.Types.ObjectId,ref:'City'},
	isfirst_time:{type:Boolean,default:true},
	status:{type:String,defautl:"Disponible"},
	tipsHogar: { type: Array, default: [
		{
			name:'hogar-1',
			src:'recomendaciones/Hogar/Hogar-1.png',
			state: false,
		  },
		  {
			name:'hogar-2',
			src:'recomendaciones/Hogar/Hogar-2.png',
			state: false,
		  },
		  {
			  name:'hogar-3',
			  src:'recomendaciones/Hogar/Hogar-3.png',
			  state: false,
			},
			{
			  name:'hogar-4',
			  src:'recomendaciones/Hogar/Hogar-4.png',
			  state: false,
			},
			{
			  name:'hogar-5',
			  src:'recomendaciones/Hogar/Hogar-5.png',
			  state: false,
			},
			{
			  name:'hogar-6',
			  src:'recomendaciones/Hogar/Hogar-6.png',
			  state: false,
			},
			{
			  name:'hogar-7',
			  src:'recomendaciones/Hogar/Hogar-7.png',
			  state: false,
			},
			{
			  name:'hogar-8',
			  src:'recomendaciones/Hogar/Hogar-8.png',
			  state: false,
			},
			{
			  name:'hogar-9',
			  src:'recomendaciones/Hogar/Hogar-9.png',
			  state: false,
			},
			{
			  name:'hogar-10',
			  src:'recomendaciones/Hogar/Hogar-10.png',
			  state: false,
			},
	] },
	tipsPos:{type: Array, default:[
		{
			name: 'pos-1',
			src:'recomendaciones/Pos/Pos-1.png',
			state: false,
		  },
		  {
			name: 'pos-2',
			src:'recomendaciones/Pos/Pos-2.png',
			state: false,
		  },
		  {
			name: 'pos-3',
			src:'recomendaciones/Pos/Pos-3.png',
			state: false,
		  },
		  {
			name: 'pos-4',
			src:'recomendaciones/Pos/Pos-4.png',
			state: false,
		  },
		  {
			name: 'pos-5',
			src:'recomendaciones/Pos/Pos-5.png',
			state: false,
		  },
		  {
			name: 'pos-6',
			src:'recomendaciones/Pos/Pos-6.png',
			state: false,
		  },
		  {
			name: 'pos-7',
			src:'recomendaciones/Pos/Pos-7.png',
			state: false,
		  },
		  {
			name: 'pos-8',
			src:'recomendaciones/Pos/Pos-8.png',
			state: false,
		  },
		  {
			name: 'pos-9',
			src:'recomendaciones/Pos/Pos-9.png',
			state: false,
		  },
		  {
			name: 'pos-10',
			src:'recomendaciones/Pos/Pos-10.png',
			state: false,
		  },
		  {
			name: 'pos-11',
			src:'recomendaciones/Pos/Pos-11.png',
			state: false,
		  }
		  
	]},
	tipsPre: {type: Array, default: [
		{
			name: 'pre-1',
			src:'recomendaciones/Pre/Pre-1.png',
			state: false,
		  },
		  {
			name: 'pre-2',
			src:'recomendaciones/Pre/Pre-2.png',
			state: false,
		  },
		  {
			name: 'pre-3',
			src:'recomendaciones/Pre/Pre-3.png',
			state: false,
		  },
		  {
			name: 'pre-4',
			src:'recomendaciones/Pre/Pre-4.png',
			state: false,
		  },
		  {
			name: 'pre-5',
			src:'recomendaciones/Pre/Pre-5.png',
			state: false,
		  },
		  {
			name: 'pre-7',
			src:'recomendaciones/Pre/Pre-7.png',
			state: false,
		  },
		  {
			name: 'pre-8',
			src:'recomendaciones/Pre/Pre-8.png',
			state: false,
		  },
		  {
			name: 'pre-9',
			src:'recomendaciones/Pre/Pre-9.png',
			state: false,
		  },
		  {
			name: 'pre-10',
			src:'recomendaciones/Pre/Pre-10.png',
			state: false,
		  },
		  {
			name: 'pre-11',
			src:'recomendaciones/Pre/Pre-11.png',
			state: false,
		  },
		  {
			name: 'pre-12',
			src:'recomendaciones/Pre/Pre-12.png',
			state: false,
		  },
		  {
			name: 'pre-13',
			src:'recomendaciones/Pre/Pre-13.png',
			state: false,
		  },
	]}
});
// UserSchema.index({username: 1, email: 2}, {unique: true});
UserSchema.index({username: 'text', email: 'text'},{unique: true});
var User = mongoose.model('User',UserSchema);
module.exports.User = User;

var PushNotificationsSchema =  Schema({
	firetoken:{ type:String, required:true },
	type:{ type:String, required:true }
});
var PushNotifications = mongoose.model('PushNotifications',PushNotificationsSchema);
module.exports.PushNotifications = PushNotifications;

var NotificationSchema =  Schema({
	firetokendata:{ 
		title:{type:String},
		text:{type:String}
	 },
	 user:{ type:Schema.Types.ObjectId, ref:'User'},
	 date:{ type:String },
	 to:{ type:Schema.Types.ObjectId, ref:'User'},
});

var Notification = mongoose.model('Notification',NotificationSchema);
module.exports.Notification = Notification;

var SpecialtiesSchema =  Schema({
	name:{type:String}
});

var Specialties = mongoose.model('Specialties',SpecialtiesSchema);
module.exports.Specialties = Specialties;

var CountrySchema =  Schema({
	name:{type:String}
});

var Country = mongoose.model('Country',CountrySchema);
module.exports.Country = Country;

var CitySchema =  Schema({
	name:{type:String}
});

var City = mongoose.model('City',CitySchema);
module.exports.City = City;


var ClinicSchema =  Schema({
	name:{type:String},
	city:{type:Schema.Types.ObjectId,ref:'City'}
});

var Clinic = mongoose.model('Clinic',ClinicSchema);
module.exports.Clinic = Clinic;
