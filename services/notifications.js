'use strict'
var admin = require('firebase-admin');
var serviceAccount = require('../config/pushfire.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
   });


exports.sendPush = function(notification,data,tokens){
  return new Promise(function(resolve, reject){
    var message = {
        notification: notification,
        data: data
      };
      
      admin.messaging().sendToDevice(tokens,message)
      .then(function(response) {
          console.log(JSON.stringify(response));
          return resolve({message:response,data:message});
      })
      .catch(function(error) {
        console.log('Error sending message:', error);
        return reject(error);
      });

  })
}